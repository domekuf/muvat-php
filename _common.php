<?php
define("_COMMON","_common.php");
/*
put in
.bash_profile
parameters
$par = [
  "ROOT" => "",
  "BACKEND" => ""
];
*/
foreach ($par as $key=>$value) {
  $par[$key] = getenv($key);
  if ($par[$key] == "") {
    $par[$key] = $_ENV[$key];
    if ($par[$key] == "") {
      echo "Please set $key to environment variable set\n";
      return;
    }
  }
}

exec('git rev-parse --verify HEAD 2> /dev/null', $output);
$hash = $output[0];
define("VERSION", $hash);
date_default_timezone_set("UTC");
$year = date("Y");

function include_css($file_name, $remote = ""){
  if(isset($remote)){
    return "<link rel=\"stylesheet\" href=\"".CONSTANT("ROOT")."$file_name.css?v=".CONSTANT("VERSION")."\">\n";
  }else{
    return "<link rel=\"stylesheet\" href=\"$remote/$file_name.css?v=".CONSTANT("VERSION")."\">\n";
  }
}

function include_js($file_name, $remote = ""){
  if(isset($remote)){
    return "<script src=\"".CONSTANT("ROOT")."$file_name.js?v=".CONSTANT("VERSION")."\"></script>\n";
  }else{
    return "<script src=\"$remote/$file_name.js?v=".CONSTANT("VERSION")."\"></script>\n";
  }
}

?>
